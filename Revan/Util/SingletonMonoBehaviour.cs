using UnityEngine;

namespace Revan.Util
{
    [AddComponentMenu("Revan/SingletonMonoBehaviour")]
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance
        {
            get
            {
                if(!instance)
                {
                    instance = (T) FindObjectOfType(typeof(T));
                    if(!instance) Debug.LogError("An instance of " + typeof(T) + " is needed in the scene, but there is none.");
                    else DontDestroyOnLoad(instance);
                }
                return instance;
            }
        }

        protected static T instance;
    }
}