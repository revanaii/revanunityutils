using UnityEngine;

namespace Revan.Util
{
    public static class RevanExtensionMethods
    {
        public static bool DestroyGOIfOtherInstanceExists<T>(this SingletonMonoBehaviour<T> instance) where T : MonoBehaviour
        {
            if(instance.InstancesCount() > 1)
            {
                Object.Destroy(instance.gameObject);
                return true;
            }
            return false;
        }
    }
}