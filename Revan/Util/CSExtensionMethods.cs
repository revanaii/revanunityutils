﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;

namespace Revan.Util
{
    public static class CSExtensionMethods
    {
        static Random random = new Random();
        static BinaryFormatter binaryFormatter = new BinaryFormatter();






        #region Generic

        public static bool IsNull<T>(this T o)
        {
            return o == null;
        }

        #endregion Generic






        #region Enum

        public static string Stringify(this Enum enumVal)
        {
            return Enum.GetName(enumVal.GetType(), enumVal);
        }

        public static T GetRandom<T>(this Enum enumVal)
        {
            var values = Enum.GetValues(typeof(T));
            return values.GetRandom<T>();
        }

        #endregion Enum






        #region StringBuilder

        public static void Clear(this StringBuilder stringBuilder)
        {
            stringBuilder.Length = 0;
        }

        public static StringBuilder Prepend(this StringBuilder sb, string content)
        {
            return sb.Insert(0, content);
        }

        #endregion






        #region ICollection

        public static bool IsEmpty<T>(this ICollection<T> collection)
        {
            return collection.Count == 0;
        }

        #endregion






        #region List

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while(n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T GetRandom<T>(this IList<T> list)
        {
            return list[random.Next(list.Count)];
        }

        public static T DeepCopy<T>(this IList<T> list)
        {
            using(var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, list);
                memoryStream.Seek(0, SeekOrigin.Begin);
                return (T) binaryFormatter.Deserialize(memoryStream);
            }
        }

        public static void AddRangeUnique<T>(this IList<T> toList, IEnumerable<T> range)
        {
            foreach(var item in range) { if(!toList.Contains(item)) { toList.Add(item); } }
        }

        public static void AddUnique<T>(this IList<T> toList, T item)
        {
            if(!toList.Contains(item)) toList.Add(item);
        }

        public static void RemoveNulls<T>(this List<T> list)
        {
            list.RemoveAll(item => item == null);
//            for(var i = list.Count - 1; i >= 0; i--)
//            {
//                if(list[i] == null) list.RemoveAt(i);
//            }
        }

        public static bool IsNullOrEmpty<T>(this IList<T> list)
        {
            return list == null || list.Count == 0;
        }

        #endregion






        #region Array

        public static T PreLast<T>(this T[] array)
        {
            return (T) (array.Length >= 2 ? array.GetValue(array.Length - 2) : null);
        }

        public static T GetRandom<T>(this T[] array)
        {
            return (T) array.GetValue(random.Next(array.Length));
        }

        public static T GetNext<T>(this T[] array, T relativeTo)
        {
            var index = Array.IndexOf(array, relativeTo);
            index++;
            if(index == array.Length) index = 0;
            return array[index];
        }

        public static T GetRandom<T>(this Array array)
        {
            return (T) array.GetValue(random.Next(array.Length));
        }

        public static T DeepCopy<T>(this Array array)
        {
            using(var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, array);
                memoryStream.Seek(0, SeekOrigin.Begin);
                return (T) binaryFormatter.Deserialize(memoryStream);
            }
        }

        public static bool IsNullOrEmpty<T>(this T[] array)
        {
            return array == null || array.Length == 0;
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static string ToBase64(this byte[] bytes)
        {
            if(bytes.IsNullOrEmpty()) return null;
            return Convert.ToBase64String(bytes);
        }

        #endregion






        #region Dictionary

        public static V GetRandom<T, V>(this Dictionary<T, V> dictionary)
        {
            return dictionary.ElementAt(random.Next(0, dictionary.Count)).Value;
        }

        public static V GetValue<T, V>(this Dictionary<T, V> dictionary, T key)
        {
            V value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

        #endregion






        #region string

        public static string SplitCamelCase(this string str)
        {
            return Regex.Replace(Regex.Replace(str, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1 $2"), @"(\p{Ll})(\P{Ll})", "$1 $2");
        }

        public static string ReplaceAllDigits(this string str, string with)
        {
            return Regex.Replace(str, @"[\d-]", with);
        }

        public static bool IsURL(this string source)
        {
            if(source.IsNullOrEmpty()) return false;
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) &&
                   (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        public static bool IsNullOrEmpty(this string s)
        {
            return s == string.Empty || s == null;
        }

        public static bool IsNullOrWhiteSpace(this string s)
        {
            return s == null || s == " " || s == "  " || s == "   ";
        }

        public static string Wrap(string value = "", int charactersToWrapAt = 35, int maxLength = 250)
        {
            if(value.IsNullOrWhiteSpace()) return "";

            value = value.Replace("  ", " ");
            var words = value.Split(' ');
            var sb = new StringBuilder();
            var currString = new StringBuilder();

            foreach(var word in words)
            {
                if(currString.Length + word.Length + 1 < charactersToWrapAt) // The + 1 accounts for spaces
                {
                    sb.AppendFormat(" {0}", word);
                    currString.AppendFormat(" {0}", word);
                }
                else
                {
                    currString.Clear();
                    sb.AppendFormat("{0}{1}", Environment.NewLine, word);
                    currString.AppendFormat(" {0}", word);
                }
            }

            if(sb.Length > maxLength) { return sb.ToString().Substring(0, maxLength) + " ..."; }

            return sb.ToString().TrimStart().TrimEnd();
        }

        #endregion






        #region float

        public static int GetSign(this float num)
        {
            return num >= 0 ? 1 : -1;
        }

        #endregion






        #region double

        public static int GetSign(this double num)
        {
            return num >= 0 ? 1 : -1;
        }

        #endregion






        #region int

        public static int GetSign(this int num)
        {
            return num >= 0 ? 1 : -1;
        }

        public static bool IsEven(this int num)
        {
            return Math.Abs(num) % 2 == 0;
        }

        public static int MakeEvenCeil(this int num)
        {
            if(!num.IsEven())
            {
                if(num > 0) num++;
                else num--;
            }
            return num;
        }

        public static int MakeEvenFloor(this int num)
        {
            if(!num.IsEven())
            {
                if(num > 0) num--;
                else num++;
            }
            return num;
        }

        public static int DigitsCount(this int num)
        {
            return (int) Math.Floor(Math.Log10(num) + 1);
        }

        #endregion
    }
}