﻿using System;

namespace Revan.Util.Editor
{
    public class EditorTimerCallback
    {
        public float timeLeft;
        public float timeTotal;
        public bool repeated;

        Action callback;

        public Action Callback
        {
            get { return callback; }
        }

        public bool Invoked
        {
            get { return invoked; }
        }

        bool invoked;

        public void Invoke()
        {
            if(!invoked || repeated)
            {
                callback.Invoke();
                invoked = !repeated;
                if(repeated) timeLeft = timeTotal;
            }
        }

        public EditorTimerCallback(Action callback, float timeTotal, bool repeated = false)
        {
            this.callback = callback;
            this.timeTotal = timeLeft = timeTotal;
            this.repeated = repeated;
        }

        public void Advance(float deltaTime)
        {
            timeLeft -= deltaTime;
            if(timeLeft <= 0) Invoke();
        }
    }
}