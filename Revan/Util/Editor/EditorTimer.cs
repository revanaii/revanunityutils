﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Revan.Util.Editor
{
    //TODO make it compatible with EgoCS
    [ExecuteInEditMode]
    public class EditorTimer : SingletonAutoInstansiatableMonoBehavior<EditorTimer>
    {
        List<EditorTimerCallback> callbacks = new List<EditorTimerCallback>();
        const int MaxCallbacks = 100;
        float lastEditorUpdateTime;


        void OnEnable()
        {
            #if UNITY_EDITOR
            lastEditorUpdateTime = Time.realtimeSinceStartup;
            EditorApplication.update += OnEditorUpdate;
            #endif
        }

        void OnDisable()
        {
            #if UNITY_EDITOR
            EditorApplication.update -= OnEditorUpdate;
            #endif
        }

        void OnEditorUpdate()
        {
            var deltaTime = Time.realtimeSinceStartup - lastEditorUpdateTime;
            for(int i = 0; i < callbacks.Count; i++)
            {
                var editorCallback = callbacks[i];
                editorCallback.Advance(deltaTime);
                if(editorCallback.Invoked && !editorCallback.repeated) callbacks.Remove(editorCallback);
            }
            lastEditorUpdateTime = Time.realtimeSinceStartup;
        }

        public void Invoke(Action callback, float time, bool repeated = false)
        {
            if(callbacks.Count < MaxCallbacks)
            {
                var editorCallback = new EditorTimerCallback(callback, time, repeated);
                callbacks.Add(editorCallback);
            }
            else
            {
                Debug.LogWarning("EditorTimer.Invoke too many callbacks, MaxCallbacks="+MaxCallbacks);
            }
        }

        public void CancelInvoke(Action callback)
        {
            var editorCallback = callbacks.Find(x => x.Callback == callback);
            callbacks.Remove(editorCallback);
        }

    }
}