﻿using System.Collections.Generic;
using System.Reflection;

namespace Revan.Util
{
    public static class ReflectionUtils
    {
        public static List<TField> GetClassStaticFieldsValues<TClass, TField>()
        {
            var result = new List<TField>();
            var classType = typeof(TClass);
            var fieldType = typeof(TField);
            foreach (var field in classType.GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                var value = field.GetValue(null);
                if(value != null && value.GetType() == fieldType)
                {
                    result.Add((TField)value);
                }
            }
            return result;
        }
    }
}