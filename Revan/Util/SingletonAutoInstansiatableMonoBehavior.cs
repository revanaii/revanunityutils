﻿using UnityEngine;

namespace Revan.Util
{
    //TODO make it compatible with EgoCS
    public class SingletonAutoInstansiatableMonoBehavior<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance
        {
            get
            {
                if(!instance)
                {
                    instance = (T) FindObjectOfType(typeof(T));
                    if(!instance) instance = new GameObject(typeof(T).Name).AddComponent<T>();
                    DontDestroyOnLoad(instance.gameObject);
                }
                return instance;
            }
        }

        protected static T instance;
    }
}