﻿using UnityEngine;

namespace Revan.Util
{
    public static class UnityJsonUtil
    {
        public static T[] ParseToArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.array;
        }

        [System.Serializable]
        class Wrapper<T>
        {
            public T[] array = null;
        }
    }
}