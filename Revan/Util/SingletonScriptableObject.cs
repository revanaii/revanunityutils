using System.Linq;
using UnityEngine;


namespace Revan.Util
{
    /// <summary>
    /// ВАЖНО: нужно перетащить созданный ассет вручную на какой-то геймобжект на сцене,
    /// иначе все наебнется
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SingletonScriptableObject<T> : ScriptableObject where T : ScriptableObject
    {
        protected static T instance;

        public static T Instance
        {
            get
            {
                if(!instance) instance = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
                return instance;
            }
        }
    }
}