using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Revan.Util
{
    public static class UnityExtensionMethods
    {
        #region Transform

        public static void LookAt2D(this Transform transform, Transform target)
        {
            Vector2 dir = target.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        public static void LookAt2D(this Transform transform, Vector2 point)
        {
            Vector2 dir = point - (Vector2) transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        public static bool HasChild(this Transform transform, string name)
        {
            if(transform.IsNull()) return false;
            for(int i = 0; i < transform.childCount; i++)
            {
                if(transform.GetChild(i).name.Equals(name)) { return true; }
            }
            return false;
        }

        public static void DestroyChildren(this Transform parent)
        {
            if(parent.IsNull()) return;
            int childCount = parent.childCount;
            for(int i = childCount - 1; i > -1; i--) { Object.DestroyObject(parent.GetChild(i).gameObject); }
        }

        public static void MoveChildren(this Transform oldParent, Transform newParent)
        {
            if(oldParent.IsNull() || newParent.IsNull()) return;
            int childCount = oldParent.childCount;
            for(int i = 0; i < childCount; i++) { oldParent.GetChild(0).SetParent(newParent, false); }
        }

        #endregion






        #region GameObject

        //<summary>http://answers.unity3d.com/questions/555101/possible-to-make-gameobjectgetcomponentinchildren.html</summary>
        ///////////////////////////////////////////////////////////
        // Essentially a reimplementation of
        // GameObject.GetComponentInChildren< T >()
        // Major difference is that this DOES NOT skip deactivated
        // game objects
        ///////////////////////////////////////////////////////////
        public static TType GetComponentInChildren<TType>(this GameObject objRoot, bool includeInactive)
            where TType : Component
        {
            if(includeInactive)
            {
                // if we don't find the component in this object
                // recursively iterate children until we do
                TType tRetComponent = objRoot.GetComponent<TType>();

                if(null == tRetComponent)
                {
                    // transform is what makes the hierarchy of GameObjects, so
                    // need to access it to iterate children
                    Transform trnsRoot = objRoot.transform;
                    int iNumChildren = trnsRoot.childCount;

                    // could have used foreach(), but it causes GC churn
                    for(int iChild = 0; iChild < iNumChildren; ++iChild)
                    {
                        // recursive call to this function for each child
                        // break out of the loop and return as soon as we find
                        // a component of the specified type
                        tRetComponent = trnsRoot.GetChild(iChild).gameObject.GetComponentInChildren<TType>(true);
                        if(null != tRetComponent) { break; }
                    }
                }

                return tRetComponent;
            }
            else { return objRoot.GetComponentInChildren<TType>(); }
        }

        #endregion GameObject






        #region UI Text

        public static void SetText(this Text text, object t)
        {
            if(text.IsNull() || t.IsNull()) { return; }
            text.text = t.ToString();
        }

        #endregion UI Text






        #region UI Image

        public static void SetTexture2D(this Image image, Texture2D texture)
        {
            if(image.IsNull() || texture.IsNull()) return;
            image.overrideSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5F, 0.5F));
        }

        public static void SetAlpha(this Image image, float value)
        {
            Color c = image.color;
            c.a = value;
            image.color = c;
        }

        #endregion UI Image






        #region UI ScrollRect

        public static void ScrollToBottom(this ScrollRect scrollRect, float timeDelay = 0.05f)
        {
            scrollRect.ScrollTo(0, timeDelay);
        }

        public static void ScrollToTop(this ScrollRect scrollRect, float timeDelay = 0.05f)
        {
            scrollRect.ScrollTo(1, timeDelay);
        }

        public static void ScrollTo(this ScrollRect scrollRect, float value, float timeDelay = 0.05f)
        {
            //без паузы не успевает учесть новый размер лейаута
            if(scrollRect.vertical)
                Invoke(scrollRect, () => { scrollRect.verticalNormalizedPosition = value; }, timeDelay);
            if(scrollRect.horizontal)
                Invoke(scrollRect, () => { scrollRect.horizontalNormalizedPosition = value; }, timeDelay);
        }

        #endregion UI ScrollRect






        #region Sprite serialization

        public static Sprite ToSprite(this byte[] bytes, TextureFormat format, int width = 2, int height = 2,
            bool mipmap = false)
        {
            if(bytes.IsNull() || bytes.Length == 0 || width < 0 || height < 0) { return null; }
            Texture2D texture = new Texture2D(width, height, format, false);
            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5F, 0.5F));
        }

        public static byte[] ToBytesPNG(this Sprite sprite)
        {
            if(sprite.IsNull()) { return null; }
            return sprite.texture.EncodeToPNG();
        }


        public static byte[] ToBytesJPG(this Sprite sprite)
        {
            if(sprite.IsNull()) { return null; }
            return sprite.texture.EncodeToJPG();
        }

        public static Sprite ToSprite(this string base64, TextureFormat format, int width = 2, int height = 2,
            bool mipmap = false)
        {
            if(base64.IsNullOrEmpty()) { return null; }
            return Convert.FromBase64String(base64).ToSprite(format, width, height, mipmap);
        }

        #endregion Sprite serialization






        #region MonoBehavior

        public static void SetGOActive(this MonoBehaviour monoBehaviour, bool value)
        {
            monoBehaviour.gameObject.SetActive(value);
        }

        public static Coroutine Invoke(this MonoBehaviour monoBehaviour, Action action, float time)
        {
            return monoBehaviour.StartCoroutine(InvokeImpl(action, time));
        }

        static IEnumerator InvokeImpl(Action action, float time)
        {
            yield return new WaitForSeconds(time);
            action();
        }

        #endregion






        #region Object

        public static String GetNamespaceFirstPart(this System.Object obj)
        {
            string ns = obj.GetType().Namespace;
            if(ns != null) return ns.Split('.')[0];
            return String.Empty;
        }

        public static int InstancesCount(this System.Object obj)
        {
            var type = obj.GetType();
            var instances = Object.FindObjectsOfType(type);
            return instances.Length;
        }

        #endregion






        #region RangeInt

        public static int GetRandom(this RangeInt rangeInt)
        {
            return UnityEngine.Random.Range(rangeInt.start, rangeInt.end);
        }

        public static int GetHalf(this RangeInt rangeInt)
        {
            return rangeInt.length / 2;
        }

        #endregion
    }
}