using System;

namespace Revan.Util
{
    [Serializable]
    public struct RangeFloat
    {
        public float start;
        public float length;

        public float End
        {
            get { return start + length; }
        }

        /// <summary>
        ///   <para>Like UnityEngine.RangeInt, but float</para>
        /// </summary>
        public RangeFloat(float start, float length)
        {
            this.start = start;
            this.length = length;
        }
    }
}